terraform {
  required_version = "~> 1.3"

  backend "s3" {
    bucket = "npdterraformbackend"
    key    = "east/terraform.state"
    region = "us-east-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

module "networking" {
  source        = "./modules/networking"
  allocation_id = "eipalloc-e2583787"
}

module "compute" {
  source = "./modules/compute"

  master_type               = "t2.micro"
  slave_type                = "t2.micro"
  instance_ami              = "ami-0149b2da6ceec4bb0"
  availability_zone         = "us-east-1a"
  key_name                  = "AutomatizacionDevops"
  environment               = "development"
  public_subnet_id          = module.networking.public_subnet
  public_security_group_id  = [module.networking.public_security_group]
  private_subnet_id         = module.networking.private_subnet
  private_security_group_id = [module.networking.private_security_group]
  master_instance_count     = 1
  slave_instance_count      = 2
}
