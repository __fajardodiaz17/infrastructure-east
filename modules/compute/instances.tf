resource "aws_instance" "infos_master_instance"{
  instance_type               = var.master_type
  ami                         = var.instance_ami
  associate_public_ip_address = true
  availability_zone           = var.availability_zone
  ebs_block_device {
    delete_on_termination = true
    device_name           = "/dev/sda1"
    volume_size           = "30"
    volume_type           = "gp2"
  }
  key_name        = var.key_name
  subnet_id = var.public_subnet_id
  vpc_security_group_ids = var.public_security_group_id

  count = var.master_instance_count

  tags = {
    Name        = "Master"
    Environment = var.environment
  }
}

resource "aws_instance" "infos_slave_instance"{
  instance_type     = var.slave_type
  ami               = var.instance_ami
  availability_zone = var.availability_zone
  ebs_block_device {
    delete_on_termination = true
    device_name           = "/dev/sda1"
    volume_size           = "30"
    volume_type           = "gp2"
  }
  count = var.slave_instance_count
  key_name        = var.key_name
  subnet_id       = var.private_subnet_id
  vpc_security_group_ids = var.private_security_group_id

  tags = {
    Name        = "Slave"
    Environment = var.environment
  }
}