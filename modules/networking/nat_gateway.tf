resource "aws_nat_gateway" "infos_nat_gateway" {
    allocation_id = var.allocation_id
    subnet_id = aws_subnet.infos_public_subnet.id

    tags = {
        Environment = var.environment,
        Name = "NPD NAT Gateway"
    }

    depends_on = [ aws_internet_gateway.infos_internet_gateway ]
}