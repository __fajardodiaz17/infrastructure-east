resource "aws_route_table_association" "associate_public_subnet_rt"{
    subnet_id = aws_subnet.infos_public_subnet.id
    route_table_id = aws_route_table.infos_public_rt.id 
}

resource "aws_route_table_association" "associate_private_subnet_rt"{
    subnet_id = aws_subnet.infos_private_subnet.id
    route_table_id = aws_route_table.infos_private_rt.id 
}