# Default security group for the private subnet
resource "aws_security_group" "infos_vpc_security_group_private" {
  name        = "Security group for private subnet"
  description = "Default SG to alllow traffic from the VPC"
  vpc_id      = aws_vpc.infos_vpc.id

  ingress {
    description = "SSH only from internal VPC clients"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["11.0.0.0/16"]
  }

  ingress {
    description = "All traffic from the subnet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [aws_subnet.infos_public_subnet.cidr_block]
  }

  ingress {
    description = "All traffic from own subnet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "NPD Security Group"
    Environment = var.environment
  }
  
  depends_on  = [aws_vpc.infos_vpc]
}

# Default security group for the public subnet
resource "aws_security_group" "infos_vpc_security_group_public" {
  name        = "Security group for public subnet"
  description = "Default SG to allow ssh from anywhere"
  vpc_id      = aws_vpc.infos_vpc.id

  ingress {
    description = "SSH from anywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Jenkins from anywhere"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Master"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    # MY IP
    cidr_blocks = ["190.218.197.49/32"]
  }

  ingress {
    description = "All traffic from the subnet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [aws_subnet.infos_private_subnet.cidr_block]
  }

  ingress {
    description = "All traffic from security group"
    from_port = 0
    to_port = 0
    protocol = "-1"
    security_groups = [ aws_security_group.infos_vpc_security_group_private.id ]
  }

  ingress {
    description = "All traffic from own subnet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "NPD Security Group Public Subnet"
    Environment = var.environment
  }

  depends_on  = [aws_vpc.infos_vpc]
}