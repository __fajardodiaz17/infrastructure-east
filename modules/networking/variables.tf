variable "environment" {
  type        = string
  default     = "development"
  description = "Environment for aws networking"
}

variable "availability_zone" {
  type        = string
  default     = "us-east-1a"
  description = "A-Z Zone"
}

variable "allocation_id" {
  type        = string
  description = "Elastic IP for nat gateway"
}

